﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerController : MonoBehaviour
{
    PuzzleFader fader;
    private Rigidbody2D rb;
    private Animator animator;
    //public TextMeshProUGUI vidas;

    public float acceleation_value = 0.02f; //para q sea tuneable el q tan rapido acelera

    private float acceleration;//Uso esto para evitar que el personaje empiece a su maxima velocidad
    private Vector2 direction;
    private Vector2 aimDirection;
    public float attackChargeTime;
    public float attackRecoveryTime;

    [SerializeField]
    public float impulseAttackForce = 100;
    [SerializeField]
    public float movementSpeed = 400;
    [SerializeField]
    public float attackRange = 2;
    public float attackAngle = 90;

    [SerializeField]
    private float health = 4;
    [SerializeField]
    private float invulnerableTime = 2.0f;
    private float timer_invulnerable;

    [SerializeField]
    public float meleeKnockbackForce = 300;
    [SerializeField]
    public int meleeDamage = 1;
    [SerializeField]
    public int rangeDamage = 1;
    [SerializeField]
    public float arrowLifeSpan = 2;

    public bool useBow = false;
    public GameObject arrowPrefab;
    [SerializeField]
    public float arrowSpeed = 1200;
    public LayerMask enemyLayerMask;

    private Vector2 attackerPosition;

    public GameObject hookPrefab;
    [SerializeField]
    public float hookSpeed = 2000;

    [SerializeField]
    public float hookMovementSpeed = 5000;

    public GameObject aim;
    public GameObject camera_guide;
    public float cameraLookUpRange = 5.0f;

    private Vector2 velocity;//Tuve que crear esto para pasarselo al fixed update y que no se rompan las fisicas.

    [HideInInspector]
    public LifeRunes lifeRunes;
    bool invulnerable = false;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        fader = FindObjectOfType<PuzzleFader>();
        //lifeRunes = FindObjectOfType<LifeRunes>();
        lifeRunes.refillHealt();
        WwiseWrapper.SoundEvent("ambiente", gameObject);
        AkSoundEngine.SetSwitch("musica_ingame", "main", gameObject);
        //WwiseWrapper.SoundEvent("music_main", gameObject);
    }

    void Update()
    {
        Animations();
        Invulnerable();
    }

    void FixedUpdate()
    {
        rb.velocity = velocity * Time.fixedDeltaTime;
    }

    void GetInput()
    {
        direction = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        direction.Normalize(); //*La velocidad diagonal creo que esta siendo la suma de la horizontal y vertical 

        if (direction != Vector2.zero)
        {
            if (direction.x != 0)
            {
                aimDirection = new Vector2(direction.x, 0);
            }
            else
            {
                aimDirection = new Vector2(0, direction.y);
            }
            acceleration += acceleation_value;
        }
        else
        {
            acceleration--;
        }
        acceleration = Mathf.Clamp(acceleration, 0, 2);
    }

    public void IdleAndMoveBehaviours()
    {
        if (Input.GetButtonDown("Attack"))
        {
            useBow = false;
            animator.SetBool("Attacking", true);
        }
        if (Input.GetButtonDown("Hook"))
        {
            animator.SetBool("Hook", true);
            SpawnHook();
        }
        if (Input.GetButtonDown("ChangeWeapon"))
        {
            WwiseWrapper.SoundEvent("arco_cargando", gameObject);
        }
        if (Input.GetButtonUp("ChangeWeapon"))
        {
            //useBow = !useBow;
            useBow = true;
            animator.SetBool("Attacking", true);
        }
        GetInput();
        Move();
    }

    public void AttackImpulse()
    {
        if(!useBow)
          velocity = aimDirection.normalized * impulseAttackForce;
    }

    public void Move()
    {
        velocity = direction * movementSpeed * (acceleration);

        if (velocity.sqrMagnitude > 0.0f)
        {
            PasosTimer();
        }
        
        Aim();
        CameraGuide();
    }

    public float tiempoPasos = 0.5f;
    float pasos_Timer;
    void PasosTimer()
    {
        pasos_Timer -= Time.deltaTime;

        if(pasos_Timer<= 0.0f)
        {
            pasos_Timer = tiempoPasos;
            WwiseWrapper.SoundEvent("pasos", gameObject);
        }

    }

    public void MoveToHook(Vector3 hookPosition)
    {
        Vector3 direction = hookPosition - transform.position;
        velocity = direction.normalized * hookMovementSpeed;
    }

    public void Knockback()
    {
        Vector3 direction = transform.position - new Vector3(0, 1.5f, 0) - (Vector3)attackerPosition;
        velocity = direction.normalized * movementSpeed;
    }

    public void Freeze()
    {
        velocity = Vector2.zero;
    }

    void CameraGuide()
    {
        camera_guide.transform.localPosition = direction * cameraLookUpRange;
    }

    void Aim()
    {
        aim.transform.localPosition = aimDirection.normalized * attackRange;
    }

    public void ManageAttack()
    {
        if (useBow)
        {
            BowAttack();
            WwiseWrapper.SoundEvent("arco_ataque", gameObject);
        }
        else
        {
            MeleeAttack();
            if(Random.value>0.5f)
                WwiseWrapper.SoundEvent("ataque_melee_1", gameObject);
            else
                WwiseWrapper.SoundEvent("ataque_melee_2", gameObject);
        }
    }

    void SpawnHook()
    {
        WwiseWrapper.SoundEvent("gancho_desplegado", gameObject);
        GameObject hook = Instantiate(hookPrefab, transform.position, Quaternion.identity, null);
        //hook.transform.up = aimDirection - (Vector2)transform.position;
        hook.GetComponent<Hook>().velocity = aimDirection.normalized * hookSpeed;
        hook.transform.right = aimDirection.normalized;
    }

    void MeleeAttack()
    {
        //Collider2D[] enemies = Physics2D.OverlapCircleAll(aim.transform.position, attackRadius, enemyLayerMask, -Mathf.Infinity, Mathf.Infinity);
        Collider2D[] enemies = Physics2D.OverlapCircleAll(transform.position, attackRange, enemyLayerMask, -Mathf.Infinity, Mathf.Infinity);

        if (enemies.Length > 0)
        {

            foreach (Collider2D enemy in enemies)
            {
                if(Vector3.Angle(aim.transform.position-transform.position, 
                    enemy.transform.position - transform.position) <= attackAngle/2)
                {
                    enemy.GetComponent<Enemy>().ReduceHealth(meleeDamage, transform.position, meleeKnockbackForce);

                }
            }
        }
    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        //Gizmos.color = Color.red;
        //Gizmos.DrawSphere(aim.transform.position, attackRadius);
        UnityEditor.Handles.DrawSolidArc(transform.position, transform.forward, aim.transform.position - transform.position, attackAngle / 2, attackRange);
        UnityEditor.Handles.DrawSolidArc(transform.position, transform.forward, aim.transform.position - transform.position, -attackAngle / 2, attackRange);
    }
#endif

    void BowAttack()
    {
        GameObject arrow = Instantiate(arrowPrefab, transform.position, Quaternion.identity, null);
        //arrow.transform.up = aimDirection - (Vector2)transform.position;
        arrow.transform.right = aimDirection.normalized;
        arrow.GetComponent<Arrow>().velocity = aimDirection.normalized * arrowSpeed;
        arrow.GetComponent<Arrow>().damage = rangeDamage;
        arrow.GetComponent<Arrow>().lifeSpan = arrowLifeSpan;
    }

    public void ReduceHealth(int damage, Vector2 position)
    {
        if (invulnerable) return;

        health -= damage;
        WwiseWrapper.SoundEvent("damage_received", gameObject);

        if (health <= 0)
        {
            fader.GameOver();
            print("Moriste :P");
        }

        //vidas.text = "Vidas:" + health;
        lifeRunes.reduceHealt(health);

        attackerPosition = position;
        animator.SetBool("Knockback", true);

        invulnerable = true;
        timer_invulnerable = invulnerableTime;
        //Debug.Log("Invunerable!");
    } 

    void Invulnerable()
    {
        if (invulnerable)
        {
            timer_invulnerable -= Time.deltaTime;

            if(timer_invulnerable <= 0.0f)
            {
                invulnerable = false;
                //Debug.Log("normal");
            }
        }
    }

    void Animations()
    {
        if(direction != Vector2.zero)
        {
            animator.SetFloat("Horizontal", direction.x);
            animator.SetFloat("Vertical", direction.y);
        }
        animator.SetFloat("Magnitude", direction.magnitude);
    }

}
