﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public enum VoiceType
{
    mudo,
    mujer,
    hombre,
    agudo,
    crowd
}

public class Npc : MonoBehaviour
{
    public VoiceType tipoDeVoz;

    Animator anim;
    DialogueBox diagbox;

    [SerializeField]
    string npc_name = "";

    int vecesCharla = 0;

    [TextArea]
    [SerializeField]
    string npc_message1;

    public bool dosMensajes = false;

    [TextArea]
    [SerializeField]
    string npc_message2;

    PlayerController player;
    bool playerCanTalk = false;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        diagbox = FindObjectOfType<DialogueBox>();
        player = FindObjectOfType<PlayerController>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            WwiseWrapper.SoundEvent("chat_acercarse", gameObject);
            playerCanTalk = true;
        }
    }

    private void Update()
    {
        if (!playerCanTalk) return;

        if (Input.GetButtonDown("Attack"))
        {
            TalkToPlayer();
            TalkVoiceSound(tipoDeVoz);
        }
    }

    private void TalkToPlayer()
    {
        Vector2 lookDirection = player.transform.position - transform.position;
        lookDirection.Normalize();
        anim.SetFloat("Horizontal", lookDirection.x);
        anim.SetFloat("Vertical", lookDirection.y);

        if (dosMensajes)
        {
            switch (vecesCharla)
            {
                case 0:
                    diagbox.ManageDialogue(npc_name, npc_message1);
                    vecesCharla++;
                    break;
                case 1:
                    diagbox.ManageDialogue(npc_name, npc_message2);
                    vecesCharla++;
                    break;
                case 2:
                    diagbox.ManageDialogue(npc_name, npc_message2);
                    vecesCharla++;
                    break;
                case 3:
                    diagbox.ManageDialogue(npc_name, npc_message1);
                    vecesCharla++;
                    break;
                default:
                    diagbox.ManageDialogue(npc_name, npc_message2);
                    vecesCharla++;
                    break;
            }
        }
        else
        {
            diagbox.ManageDialogue(npc_name, npc_message1);
        }
        
    }


    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerCanTalk = false;

            anim.SetFloat("Horizontal", 0.0f);
            anim.SetFloat("Vertical", -1.0f);

            diagbox.HideDialogueBox();
        }
    }

    void TalkVoiceSound(VoiceType voz)
    {
        switch (voz)
        {
            case VoiceType.mudo:
                WwiseWrapper.SoundEvent("",gameObject);
                break;
            default:
                WwiseWrapper.SoundEvent("chat_" + voz.ToString(), gameObject); 
                break;
        }
    }
}