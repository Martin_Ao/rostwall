﻿using UnityEngine;

public class LifeFiller : MonoBehaviour
{
    LifeRunes lifeRunes;
    DialogueBox diagbox;

    // Si es false solo cura una sola vez
    public bool curarVariasVeces = false;
    bool yaHizoCuracion = false;

    // Si es true, cura solamente con acercarse
    public bool hablarParaCurar = false;

    public bool mostrarMensajeAlCurar = true;

    [SerializeField]
    string name = "Fuente de Vida";

    [TextArea]
    [SerializeField]
    string message = "...curando...";


    PlayerController player;
    bool playerCanTalk = false;

    private void Awake()
    {
        lifeRunes = FindObjectOfType<LifeRunes>();
        diagbox = FindObjectOfType<DialogueBox>();
        player = FindObjectOfType<PlayerController>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if(lifeRunes == null)
                lifeRunes = FindObjectOfType<LifeRunes>();

            if (!hablarParaCurar)
            {
                Curar();
                return;
            }

            playerCanTalk = true;
        }
    }

    private void Update()
    {
        if (!playerCanTalk) return;

        if (Input.GetButtonDown("Attack"))
        {
            if (!curarVariasVeces && yaHizoCuracion)
            {
                diagbox.HideDialogueBox();
                return;
            }

            Curar();
            TalkToPlayer();
        }
    }

    private void Curar()
    {
        lifeRunes.refillHealt();
        yaHizoCuracion = true;
    }

    private void TalkToPlayer()
    {
        if(mostrarMensajeAlCurar)
        {
            diagbox.ManageDialogue(name, message);
        }
    }


    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerCanTalk = false;

            diagbox.HideDialogueBox();

            if (!curarVariasVeces && yaHizoCuracion)
                this.enabled = false;
        }
    }

}
