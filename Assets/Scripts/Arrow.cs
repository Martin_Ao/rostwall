﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    public int damage;
    public float lifeSpan;
    public Vector2 velocity;
    private Rigidbody2D rb;

    [SerializeField]
    private float knockbackForce = 200;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        rb.velocity = velocity * Time.fixedDeltaTime;
    }

    private void Update()
    {
        if(lifeSpan > 0)
        {
            lifeSpan -= Time.deltaTime;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            other.gameObject.GetComponent<Enemy>().ReduceHealth(damage, transform.position, knockbackForce);
            Destroy(gameObject);
            WwiseWrapper.SoundEvent("flecha_impacta", gameObject);
        }
        if (other.gameObject.CompareTag("Wall"))
        {
            Destroy(gameObject);
            WwiseWrapper.SoundEvent("flecha_impacta", gameObject);
        }
    }
}
