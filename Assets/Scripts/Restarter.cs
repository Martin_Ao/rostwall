﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restarter : MonoBehaviour
{
    static Restarter R;

    private void Awake()
    {
        if (R == null)
            R = this;
        else
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.F12))
        {
            SceneManager.LoadScene(0);
        }
    }
}
