﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hook : MonoBehaviour
{
    public Vector2 velocity;
    private Rigidbody2D rb;
    TrailRenderer trail;

    private void Awake()
    {
        rb = GetComponent <Rigidbody2D>();
        trail = GetComponentInChildren<TrailRenderer>();
    }

    private void OnEnable()
    {
        trail.Clear();
    }
    private void OnDisable()
    {
        trail.Clear();
    }

    private void Start()
    {
        rb.velocity = velocity * Time.fixedDeltaTime;//Fix para bug
    }

    private void FixedUpdate()
    {
        rb.velocity = velocity * Time.fixedDeltaTime;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("HookTest"))
        {
            velocity = Vector2.zero;
            WwiseWrapper.SoundEvent("gancho_impacta",gameObject);
        }
        if (other.gameObject.CompareTag("Wall"))
        {
            Destroy(gameObject);
        }
    }
}
