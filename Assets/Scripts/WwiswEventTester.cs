﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WwiswEventTester : MonoBehaviour
{
    public enum TestEvents
    {
        ambiente,
        arco_ataque,
        arco_cargando,
        ataque_melee_1,
        ataque_melee_2,
        chat_acercarse,
        chat_agudo,
        chat_crowd,
        chat_comienzo,
        chat_fin,
        chat_hombre,
        chat_mujer,
        damage_received,
        enemy_attack,
        enemy_charge,
        enemy_cooldown,
        enemy_damage,
        enemy_step,
        enemy2_attack,
        enemy2_charge,
        enemy2_cooldown,
        enemy2_damage,
        enemy2_step,
        flecha_impacta,
        gancho_desplegado,
        gancho_retractando,
        gancho_viajando,
        menu,
        menu_close,
        menu_newObj,
        menu_obj,
        menu_open,
        menu_roll,
        menu_select,
        music_combat,
        music_contemplative,
        music_conversation,
        music_danger,
        music_emotive,
        music_main,
        music_sad,
        Pasos,

        Hook,
        Attack,
        BgMusic
    }

    //public TestEvents test = TestEvents.BgMusic;
    void Update()
    {
        /*
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.O))
        {
            test--;
            Debug.Log(test.ToString() + ((int)test).ToString());
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            test++;
            Debug.Log(test.ToString() + ((int)test).ToString());
        }
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            WwiseWrapper.SoundEvent(test.ToString(),gameObject);
            Debug.Log(test.ToString() + ((int)test).ToString() + "PLAYED!!!");
        }
#endif
*/
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            //WwiseWrapper.SoundEvent(TestEvents.music_combat.ToString(), gameObject);
            AkSoundEngine.SetSwitch("musica_ingame","combat",gameObject);
            Debug.Log("sdasdasd");
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            //WwiseWrapper.SoundEvent(TestEvents.music_contemplative.ToString(), gameObject);
            AkSoundEngine.SetSwitch("musica_ingame", "contemplative", gameObject);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            //WwiseWrapper.SoundEvent(TestEvents.music_conversation.ToString(), gameObject);
            AkSoundEngine.SetSwitch("musica_ingame", "conversation", gameObject);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            //WwiseWrapper.SoundEvent(TestEvents.music_danger.ToString(), gameObject);
            AkSoundEngine.SetSwitch("musica_ingame", "danger", gameObject);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            //WwiseWrapper.SoundEvent(TestEvents.music_emotive.ToString(), gameObject);
            AkSoundEngine.SetSwitch("musica_ingame", "emotive", gameObject);
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            //WwiseWrapper.SoundEvent(TestEvents.music_main.ToString(), gameObject);
            AkSoundEngine.SetSwitch("musica_ingame", "main", gameObject);
        }
        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            //WwiseWrapper.SoundEvent(TestEvents.music_sad.ToString(), gameObject);
            AkSoundEngine.SetSwitch("musica_ingame", "sad", gameObject);
        }
        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            AkSoundEngine.SetSwitch("musica_ingame", "silence", gameObject);
        }
    }
}
