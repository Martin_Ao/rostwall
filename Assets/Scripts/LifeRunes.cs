﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeRunes : MonoBehaviour
{
    Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        FindObjectOfType<PlayerController>().lifeRunes = this;
    }
    public void reduceHealt(float health)
    {
        anim.SetFloat("lifes", health);
        anim.SetTrigger("lose");
    }

    public void refillHealt()
    {
        anim.SetFloat("lifes", 4.0f);
        anim.SetTrigger("refill");
    }
}
