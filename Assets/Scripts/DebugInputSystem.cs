﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DebugInputSystem : MonoBehaviour
{
    public PlayerController player;
    public bool showDebugMode = false;
    public GameObject DebugScreen;
    public TMP_InputField aceleracion;
    public TMP_InputField velocidad;
    public TMP_InputField velocidadConGancho;
    public TMP_InputField velocidadImpulsoAtaque;
    public TMP_InputField dañoMelee;
    public TMP_InputField dañoRango;
    public TMP_InputField velocidadFlecha;
    public TMP_InputField lifespanFlecha;
    public TMP_InputField rangoMelee;
    public TMP_InputField knockbackMelee;
    public TMP_InputField tiempoCargaAtaque;
    public TMP_InputField tiempoRecuperacionAtaque;
    public TMP_InputField velocidadGancho;

    private void Awake()
    {
        player = FindObjectOfType<PlayerController>();
    }

    void Start()
    {
        aceleracion.text = player.acceleation_value.ToString();
        velocidad.text = player.movementSpeed.ToString();
        velocidadConGancho.text = player.hookMovementSpeed.ToString();
        velocidadImpulsoAtaque.text = player.impulseAttackForce.ToString();
        dañoMelee.text = player.meleeDamage.ToString();
        dañoRango.text = player.rangeDamage.ToString();
        velocidadFlecha.text = player.arrowSpeed.ToString();
        lifespanFlecha.text = player.arrowLifeSpan.ToString();
        rangoMelee.text = player.attackRange.ToString();
        knockbackMelee.text = player.meleeKnockbackForce.ToString();
        tiempoCargaAtaque.text = (player.attackChargeTime * 1000).ToString();
        tiempoRecuperacionAtaque.text = (player.attackRecoveryTime * 1000).ToString();
        velocidadGancho.text = player.hookSpeed.ToString();
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F5))
        {
            showDebugMode = !showDebugMode;
            DebugScreen.SetActive(showDebugMode);
            if (showDebugMode)
            {
                Time.timeScale = 0;
            }
            else
            {
                Time.timeScale = 1;
            }
        }
    }
    
    public void UpdateValues()
    {
        float accel;
        if (float.TryParse(aceleracion.text, out accel))
        {
            player.acceleation_value = accel;
        //PlayerPrefSaver.Save("player.acceleation_value", player.acceleation_value);
        }
        int movementSpeed;
        if (int.TryParse(velocidad.text, out movementSpeed))
        {
            player.movementSpeed = movementSpeed;
          //  PlayerPrefSaver.Save("player.movementSpeed", player.movementSpeed);
        }
        int hookMovementSpeed;
        if (int.TryParse(velocidadConGancho.text, out hookMovementSpeed))
        {
            player.hookMovementSpeed = hookMovementSpeed;
          //  PlayerPrefSaver.Save("player.hookMovementSpeed", player.hookMovementSpeed);
        }
        int impulseAttackForce;
        if (int.TryParse(velocidadImpulsoAtaque.text, out impulseAttackForce))
        {
            player.impulseAttackForce = impulseAttackForce;
          //  PlayerPrefSaver.Save("player.impulseAttackForce", player.impulseAttackForce);
        }
        int meleeDamage;
        if (int.TryParse(dañoMelee.text, out meleeDamage))
        {
            player.meleeDamage = meleeDamage;
          //  PlayerPrefSaver.Save("player.meleeDamage", player.meleeDamage);
        }
        int rangeDamage;
        if (int.TryParse(dañoRango.text, out rangeDamage))
        {
            player.rangeDamage = rangeDamage;
            //PlayerPrefSaver.Save("player.rangeDamage", player.rangeDamage);
        }
        int arrowSpeed;
        if (int.TryParse(velocidadFlecha.text, out arrowSpeed))
        {
            player.arrowSpeed = arrowSpeed;
          //  PlayerPrefSaver.Save("player.arrowSpeed", player.arrowSpeed);
        }
        int arrowLifeSpan;
        if (int.TryParse(lifespanFlecha.text, out arrowLifeSpan))
        {
            player.arrowLifeSpan = arrowLifeSpan;
          //  PlayerPrefSaver.Save("player.arrowLifeSpan", player.arrowLifeSpan);
        }
        int attackRange;
        if (int.TryParse(rangoMelee.text, out attackRange))
        {
            player.attackRange = attackRange;
           // PlayerPrefSaver.Save("player.attackRange", player.attackRange);
        }
        int meleeKnockbackForce;
        if (int.TryParse(knockbackMelee.text, out meleeKnockbackForce))
        {
            player.meleeKnockbackForce = meleeKnockbackForce;
            //PlayerPrefSaver.Save("player.meleeKnockbackForce", player.meleeKnockbackForce);
        }
        float attackChargeTime;
        if (float.TryParse(tiempoCargaAtaque.text, out attackChargeTime))
        {
            player.attackChargeTime = attackChargeTime / 1000;
            //PlayerPrefSaver.Save("player.attackChargeTime", player.attackChargeTime);
        }
        float attackRecoveryTime;
        if (float.TryParse(tiempoRecuperacionAtaque.text, out attackRecoveryTime))
        {
            player.attackRecoveryTime = attackRecoveryTime / 1000;
            //PlayerPrefSaver.Save("player.attackRecoveryTime", player.attackRecoveryTime);
        }
        int hookSpeed;
        if (int.TryParse(velocidadGancho.text, out hookSpeed))
        {
            player.hookSpeed = hookSpeed;
            //PlayerPrefSaver.Save("player.hookSpeed", player.hookSpeed);
        }
    }
}
