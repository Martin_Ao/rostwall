﻿using UnityEngine;

public static class WwiseWrapper
{
    public static void SoundEvent(string wwise_event, GameObject game_object )
    {
        if (wwise_event == "") return;
        if (wwise_event == "chat_comienzo") return;
        AkSoundEngine.PostEvent(wwise_event,game_object);
    }
}
