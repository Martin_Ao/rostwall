﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoomZone : MonoBehaviour
{
    public enum ZoomType
    {
        ZoomIn, ZoomOut
    }

    Transform player;
    public CircleCollider2D colli;

    public ZoomType zoomType = ZoomType.ZoomOut;

    Cinemachine.CinemachineVirtualCamera cam_script;

    float normalSize;
    public float zoomAmount;
    float zoomSize;

    public float zoomFinalRadius = 1.0f;
    float interiorRadius;

    bool zooming = false;
    float relativeDistance;
    float lerpi;

    private void Awake()
    {
        colli = GetComponent <CircleCollider2D>();
        cam_script = FindObjectOfType<Cinemachine.CinemachineVirtualCamera>();
    }

    private void Update()
    {
        if (zooming)
        {
            relativeDistance = Vector3.Distance(player.position,transform.position);
            lerpi = Mathf.InverseLerp(colli.radius,interiorRadius,relativeDistance);
            cam_script.m_Lens.OrthographicSize = Mathf.Lerp(normalSize,zoomSize,lerpi);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (cam_script == null)
            cam_script = FindObjectOfType<Cinemachine.CinemachineVirtualCamera>();

        if (collision.gameObject.CompareTag("Player"))
        {
            if (player == null)
                player = collision.gameObject.transform;

            zooming = true;
            normalSize = cam_script.m_Lens.OrthographicSize;
            zoomSize = normalSize;
            interiorRadius = colli.radius - zoomFinalRadius;

            zoomSize = zoomType == ZoomType.ZoomIn ?
                cam_script.m_Lens.OrthographicSize - zoomAmount :
                cam_script.m_Lens.OrthographicSize + zoomAmount ;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            zooming = false;
            cam_script.m_Lens.OrthographicSize = normalSize;
        }
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if(zoomFinalRadius < 0.0f)
        {
            zoomFinalRadius = 0.01f;
            Debug.LogWarning("El limite interior (rojo) no puede sobrepasar al exterior (verde)");
        }

        UnityEditor.Handles.color = Color.red;
        UnityEditor.Handles.DrawWireDisc(transform.position,transform.forward, colli.radius - zoomFinalRadius);
    }
#endif
}
