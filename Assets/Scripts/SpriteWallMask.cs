﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteWallMask : MonoBehaviour
{
    private void Awake()
    {
        if (GetComponent<SpriteMask>() == null)
        {
            SpriteRenderer rend = GetComponent<SpriteRenderer>();
             mask = gameObject.AddComponent<SpriteMask>();
            mask.sprite = rend.sprite;
        }
        else
            Debug.LogWarning("El objeto ya tiene añadida una mascara!");

        //Destroy(this);
    }
    SpriteMask mask;
    Transform player;
    private void Update()
    {
        if (player == null)
        {
            player = FindObjectOfType<PlayerController>().transform;
        }
        if (mask == null) return;
        if(transform.position.y >= player.transform.position.y)
        {
            mask.enabled = false;
        }
        else
        {
            mask.enabled = true;
        }
    }
}
