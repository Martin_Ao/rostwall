﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PasajeAlBosqueOscuro : MonoBehaviour
{
    int normalOrder;
    SpriteRenderer renderer;
    private void Start() {
        renderer = GameObject.FindGameObjectWithTag("Player").GetComponent<SpriteRenderer>();
        normalOrder = renderer.sortingOrder;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (renderer != null)
                renderer.sortingOrder = -1;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (renderer != null)
                renderer.sortingOrder = normalOrder;
        }
    }
}
