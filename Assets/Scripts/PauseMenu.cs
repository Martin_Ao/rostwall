﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] GameObject pause_menu = null;
    [SerializeField] Scrollbar scroll = null;

    float option = 0.0f;
    bool pause;

    PlayerController pc;

    private void Start()
    {
        pause = false;
        pause_menu.SetActive(false);
        pc = FindObjectOfType<PlayerController>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !pause)
        {
            pause = true;
            pc.enabled = !pause;
            Time.timeScale = 0.0f;
            pause_menu.SetActive(true);
            WwiseWrapper.SoundEvent("menu_open", gameObject);
        }

        if (pause)
        {
            if (Input.GetKeyUp(KeyCode.W))
            { 
                scroll.value -= 1.0f;
                WwiseWrapper.SoundEvent("menu_roll",gameObject);
            }
            if (Input.GetKeyUp(KeyCode.S))
            { 
                scroll.value += 1.0f;
                WwiseWrapper.SoundEvent("menu_roll", gameObject);
            }

            if (Input.GetKeyDown(KeyCode.J))
            {
                WwiseWrapper.SoundEvent("menu_select", gameObject);
                if (scroll.value == 0.0f)
                    Btn_Continue();
                else
                    Btn_Quit();
            }
        }
    }

    void Btn_Continue()
    {
        WwiseWrapper.SoundEvent("menu_close", gameObject);
        pause = false;
        Time.timeScale = 1.0f;
        pause_menu.SetActive(false);
        pc.enabled = !pause;
    }

    void Btn_Quit()
    {
        WwiseWrapper.SoundEvent("menu_close", gameObject);
        Debug.Log("Saliendo...");
        Application.Quit();
    }
}
