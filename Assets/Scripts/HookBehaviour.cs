﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookBehaviour : StateMachineBehaviour
{
    private Hook hook;
    private PlayerController player;

    float soundTimer = 0.5f;
    float timer = 0.0f;

    private void Awake()
    {
        player = FindObjectOfType<PlayerController>();
    }

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player.GetComponent<CircleCollider2D>().isTrigger = true;
        hook = FindObjectOfType<Hook>();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!hook)
        {
            animator.SetBool("Hook", false);
            return;
        }
        if(hook.GetComponent<Rigidbody2D>().velocity == Vector2.zero)
        {
            player.MoveToHook(hook.transform.position);
            SoundTimer(true);

            if (Vector2.Distance(animator.transform.position,hook.transform.position) < 3)
            {
                Destroy(hook.gameObject);
                animator.SetBool("Hook", false);
            }
        }
        else
        {
            SoundTimer(false);
        }
        if (Vector2.Distance(animator.transform.position, hook.transform.position) > 20)
        {
            Destroy(hook.gameObject);
            animator.SetBool("Hook", false);
        }
    }

    void SoundTimer(bool retract)
    {
        timer += Time.deltaTime;

        if(timer >= soundTimer)
        {
            timer = 0.0f;
            if (!retract)
                WwiseWrapper.SoundEvent("gancho_viajando",player.gameObject);
            else
                WwiseWrapper.SoundEvent("gancho_retractando", player.gameObject);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player.GetComponent<CircleCollider2D>().isTrigger = false;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
