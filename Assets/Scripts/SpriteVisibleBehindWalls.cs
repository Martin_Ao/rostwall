﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteVisibleBehindWalls : MonoBehaviour
{
    static Color shadeColor = new Color(0.5f, 0.5f, 0.5f, 0.35f);

    SpriteRenderer referenced;
    SpriteRenderer mimic;

    private void Awake()
    {
        referenced = GetComponent<SpriteRenderer>();
        GameObject shadeChild = new GameObject();
        shadeChild.name = "shadeChild";
        shadeChild.transform.position = transform.position;
        shadeChild.transform.rotation = transform.rotation;
        shadeChild.transform.localScale = transform.localScale;
        shadeChild.transform.parent = transform;
        mimic = shadeChild.AddComponent<SpriteRenderer>();

        referenced.maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
        mimic.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
        mimic.sortingOrder = 10000;
        mimic.color = shadeColor;
        mimic.sprite = referenced.sprite;
    }

    private void OnAnimatorMove()
    {
        mimic.sprite = referenced.sprite;
        mimic.flipX = referenced.flipX;
        mimic.flipY = referenced.flipY;
    }
}
