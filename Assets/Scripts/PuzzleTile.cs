﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleTile : MonoBehaviour
{
    public Transform returnPoint;
    static PuzzleFader fader;
    Transform player;
    static bool fading = false;
    private void Awake()
    {
        if(player == null)
        player = GameObject.FindGameObjectWithTag("Player").transform;
        if(returnPoint == null)
        {
            returnPoint = FindObjectOfType<PuzzleTile_ReturnPoint>().transform;
            fader = FindObjectOfType<PuzzleFader>();
        }
    }

    /*
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.transform.position = returnPoint.position;
        }
    }*/
    
    private void OnCollisionStay2D(Collision2D collision)
    {
        if(fading) return;
        if (collision.gameObject.CompareTag("Player"))
        {
            if (fader == null)
            {
                fader = FindObjectOfType<PuzzleFader>();
            }
            fading = true;
            fader.Relocate();
            Invoke("Relocate",.5f);
        }
    }

    void Relocate(){
        fading = false;
        player.position = returnPoint.position;
    }
}
