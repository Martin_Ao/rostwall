﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DebugEnemy : MonoBehaviour
{
    [SerializeField] GameObject enemy_prefab = null;
    [SerializeField] Transform new_enemy_position = null;

    public TMP_InputField velocidad;
    public TMP_InputField rango_ataque;
    public TMP_InputField vida;
    public TMP_InputField daño;
    public TMP_InputField knockback_speed;
    public TMP_InputField vel_embestida;
    public TMP_InputField tiempo_carga_emb;
    public TMP_InputField tiempo_embestida;
    public Slider collision_slide;

    List<Enemy> enemys = new List<Enemy>();

    private void Awake()
    {
        Enemy[] e = FindObjectsOfType<Enemy>();
        foreach(Enemy n in e)
        {
            enemys.Add(n);
        }
    }

    private void Start()
    {
        velocidad.text = enemys[0].speed.ToString();
        rango_ataque.text = enemys[0].attackRange.ToString();
        vida.text = enemys[0].health.ToString();
        daño.text = enemys[0].damage.ToString();
        knockback_speed.text = enemys[0].knockbackSpeed.ToString();
        vel_embestida.text = enemys[0].attack_power.ToString();
        tiempo_carga_emb.text = enemys[0].charge_time.ToString();
        tiempo_embestida.text = enemys[0].attack_time.ToString();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F5))
        {
            gameObject.SetActive(false);
        }
    }

    public void UpdateValues()
    {
        for (int i = 0; i < enemys.Count; i++)
        {
            try
            {
                if (!enemys[i].gameObject.activeSelf) continue;
            }
            catch
            {
                continue;
            }

            float vs;
            if (float.TryParse(velocidad.text, out vs))
            {
                enemys[i].speed = vs;
            }
            float ra;
            if (float.TryParse(rango_ataque.text, out ra))
            {
                enemys[i].attackRange = ra;
            }
            float vd;
            if (float.TryParse(vida.text, out vd))
            {
                enemys[i].health = vd;
            }
            int dmg;
            if (int.TryParse(daño.text, out dmg))
            {
                enemys[i].damage = dmg;
            }
            float kb;
            if (float.TryParse(knockback_speed.text, out kb))
            {
                enemys[i].knockbackSpeed = kb;
            }
            float ve;
            if (float.TryParse(vel_embestida.text, out ve))
            {
                enemys[i].attack_power = ve;
            }
            float tc;
            if (float.TryParse(tiempo_carga_emb.text, out tc))
            {
                enemys[i].charge_time = tc;
            }
            float te;
            if (float.TryParse(tiempo_embestida.text, out te))
            {
                enemys[i].attack_time = te;
            }
        }
    }

    public void Btn_SpawnNewEnemy()
    {
        GameObject newEnemy = Instantiate(enemy_prefab,new_enemy_position.position,Quaternion.identity);
        enemys.Add(newEnemy.GetComponent<Enemy>());
    }

    [SerializeField] GameObject enemy_panel;
    [SerializeField] GameObject edda_panel;

    public void Hide_enemyPanel()
    {
        enemy_panel.SetActive(false);
        edda_panel.SetActive(true);
    }
    public void Show_enemyPanel()
    {
        enemy_panel.SetActive(true);
        edda_panel.SetActive(false);
    }

    public void Manage_CollisionSlider()
    {
        if(collision_slide.value == 0) // Colisiones activadas
        {
            for (int i = 0; i < enemys.Count; i++)
            {
                try
                {
                    if (!enemys[i].gameObject.activeSelf) continue;
                }
                catch
                {
                    continue;
                }

                enemys[i].coli.isTrigger = false;
            }
        }
        else if(collision_slide.value == 1) // Trigger activado
        {
            for (int i = 0; i < enemys.Count; i++)
            {
                try
                {
                    if (!enemys[i].gameObject.activeSelf) continue;
                }
                catch
                {
                    continue;
                }

                enemys[i].coli.isTrigger = true;
            }
        }
    }
}
