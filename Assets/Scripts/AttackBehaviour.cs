﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackBehaviour : StateMachineBehaviour
{
    float attackTime;
    float recoverTime;
    public PlayerController player;
    private bool hasAttacked;

    private void Awake()
    {
        player = FindObjectOfType<PlayerController>();
    }

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        attackTime = player.attackChargeTime;
        recoverTime = player.attackRecoveryTime;
        hasAttacked = false;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player.AttackImpulse();
        if(attackTime > 0)
        {
            attackTime -= Time.deltaTime;
        }
        else if(!hasAttacked)
        {
            player.ManageAttack();
            hasAttacked = true;
        }
        else
        {
            if (recoverTime > 0)
            {
                recoverTime -= Time.deltaTime;
            }
            else
            {
                animator.SetBool("Attacking", false);
            }
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
