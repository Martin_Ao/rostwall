﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackBehaviour : StateMachineBehaviour
{
    float chargeTime;
    float attackTime;
    public float startAttackTime;
    public float startChargeTime;
    public Enemy enemy;
    private bool hasAttacked;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("Attacking", true);
        chargeTime = startChargeTime;
        attackTime = startAttackTime;
        enemy = animator.GetComponent<Enemy>();
        hasAttacked = false;

        startChargeTime = enemy.charge_time;
        startAttackTime = enemy.attack_time;
        enemy.ManageSound("_charge");
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (chargeTime > 0)
        {
            chargeTime -= Time.deltaTime;
        }
        else if(!hasAttacked)
        {
            enemy.ManageSound("_attack");
            enemy.Attack();
            hasAttacked = true;
        }
        else
        {
            if(attackTime > 0)
            {
                attackTime -= Time.deltaTime;
            }
            else
            {
                animator.SetBool("Attacking", false);
            }
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
