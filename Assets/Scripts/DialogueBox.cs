﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogueBox : MonoBehaviour
{
    [SerializeField] GameObject diag_box = null;
    [SerializeField] TextMeshProUGUI diag_name = null;//Puse null solamente para que no salgan las advertencias
    [SerializeField] TextMeshProUGUI diag_message = null;

    Animator player_anim;
    bool talking = false;

    [SerializeField] int chars_per_pages = 50;
    int pages = 0;
    int actual_page = 0;
    List<string> full_message = new List<string>();

    private void Awake()
    {
        player_anim = GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>();
    }

    public void ManageDialogue(string npc_name, string npc_message)
    {
        if(pages == 0)
        {
            //char[] c = npc_message.ToCharArray();

            int char_count = chars_per_pages;

            string page_message = "";

            foreach (char c in npc_message)
            {
                page_message = page_message + c + "";
                char_count--;

                if(char_count <= 0)
                {
                    pages++;
                    char_count = chars_per_pages;
                    //full_message.Add(page_message);
                    //page_message = "";
                    page_message = CheckCutMessageOnSpace(page_message, ref full_message);
                }

            }

            pages++;
            char_count = chars_per_pages;
            full_message.Add(page_message);
            page_message = "";

            talking = true;
            diag_box.SetActive(true);
            player_anim.SetBool("Talking", talking);

            WwiseWrapper.SoundEvent("chat_comienzo",gameObject);
        }

        if (actual_page < pages)
        {
            DisplayMessage(npc_name, npc_message);
        }
        else
        {
            HideDialogueBox();
        }
    }

    string CheckCutMessageOnSpace(string page_message ,ref List<string> full_message)
    {
        string cutted_message = "";
        char[] char_message = page_message.ToCharArray();

        for (int i = char_message.Length -1; i >= 0; i--)
        {
            Debug.Log(char_message[i]);
            if(char_message[i] == ' ')
            {
                if (char_message[i] == char_message.Length - 1)
                    return page_message;

                for (int j = 0; j <= i; j++)
                {
                    cutted_message += char_message[j];
                }

                full_message.Add(cutted_message); // Guarda el mensaje cortado en el 'espacio'

                cutted_message = "";

                for (int k = i + 1; k < char_message.Length; k++)
                {
                    cutted_message += char_message[k];
                }

                return cutted_message;  // Devuelve la palabra sobrante
            }
        }

        return page_message;
    }

    void DisplayMessage (string npc_name, string npc_message)
    {
        diag_name.text = npc_name;
        diag_message.text = full_message[actual_page];
        actual_page++;
    }

    public void HideDialogueBox()
    {
        pages = 0;
        actual_page = 0;
        full_message.Clear();

        talking = false;
        diag_box.SetActive(false);
        player_anim.SetBool("Talking", talking);

        WwiseWrapper.SoundEvent("chat_fin", gameObject);
    }
}
