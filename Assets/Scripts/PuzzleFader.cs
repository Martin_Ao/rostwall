﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleFader : MonoBehaviour
{
    public Animator anim;

    private void Awake()
    {
        //anim = GetComponent<Animator>();
    }

    public void Relocate()
    {
        anim.SetTrigger("relocate");
    }

    public void GameOver()
    {
        anim.SetTrigger("gameover");
    }

    public void DemoEnd()
    {
        anim.SetTrigger("demoend");
    }
}
