﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoBgMusic : MonoBehaviour
{
    public AudioSource audio;

    public void MusicPause()
    {
        if(audio == null) return;
        audio.Pause();
    }

    public void MusicContinue()
    {
        if(audio == null) return;
        audio.UnPause();
    }
}
