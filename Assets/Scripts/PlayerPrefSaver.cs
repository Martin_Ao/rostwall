﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerPrefSaver 
{
    public static void Save(string propiedad, float valor)
    {
        PlayerPrefs.SetFloat(propiedad,valor);
        PlayerPrefs.Save();
    }

    public static void Save(string propiedad, int valor)
    {
        PlayerPrefs.SetInt(propiedad, valor);
        PlayerPrefs.Save();
    }

    public static void Save(string propiedad, string valor)
    {
        PlayerPrefs.SetString(propiedad, valor);
        PlayerPrefs.Save();
    }
}
