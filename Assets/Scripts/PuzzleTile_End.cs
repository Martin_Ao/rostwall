﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class PuzzleTile_End : MonoBehaviour
{
    VideoPlayer video;
    bool playerInRange = false;
    bool ended = false;
    bool video_prepared = false;
    DemoBgMusic bgm;
    Canvas canvas;
    PuzzleFader fader;

    private void Awake()
    {
        fader = GetComponent<PuzzleFader>();
        canvas = FindObjectOfType<Canvas>();
        bgm = FindObjectOfType<DemoBgMusic>();
        Camera cam = Camera.main;
        video = GetComponent<VideoPlayer>();
        video.playOnAwake = false;
        video.targetCamera = cam;
        video.renderMode = VideoRenderMode.CameraNearPlane;
        video.Prepare();
    }

    void VideoPrepared()
    {
        Debug.Log("Video OK!");
        video_prepared = true;
        video.Pause();
    }

    private void Update()
    {
        if(!video_prepared)
          if (video.isPrepared) VideoPrepared();

        if (playerInRange && !ended)
        {
            if (Input.GetButton("Attack"))
            {
                DisplayVideo();
                ended = true;
                Time.timeScale = 0.0f;
            }
        }
        if(playing){
            timer-= Time.unscaledDeltaTime;
            if(timer<=0.0f){
                VideoEnded();
            }
        }
    }

    void DisplayVideo()
    {
        video.Play();
        canvas.gameObject.SetActive(false);
        if(bgm != null)
            bgm.MusicPause();
        playing = true;
        Debug.Log("Playing Video ...");
    }

    float timer = 16;
    bool playing = false;

    void VideoEnded()
    {
        Debug.Log("stopeando video");
        Time.timeScale = 1.0f;
        canvas.gameObject.SetActive(true);
        video.Stop();
        if(bgm != null)
            bgm.MusicContinue();
        this.enabled = false;
        fader.DemoEnd(); 
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        playerInRange = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        playerInRange = false;
    }
}
