﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Enemy : MonoBehaviour
{
    public TextMeshPro vidas;

    private Rigidbody2D rb;
    private Animator animator;
    private PlayerController player;
    //[SerializeField]
    public float speed = 100;
    //[SerializeField]
    public float attackRange = 2;
    //[SerializeField]
    //private float attackRadius = 1;
    public LayerMask playerLayerMask;

    private Vector2 attackerPosition;

    private Vector2 velocity;

    //[SerializeField]
    public float health = 3;

    //[SerializeField]
    public int damage = 1;

    public GameObject aim;

    //[SerializeField]
    public float knockbackSpeed = 100;//Velocidad con la que vas mientras estas siendo empujado por un ataque

    //[SerializeField]
    public float attack_power = 2.0f; // Velocidad durante la embestida

    bool attacking = false;

    public float charge_time = 0.5f;        // Tiempo antes de embestir
    public float attack_time = 0.5f;        // Con este y attack_ power regular la embestida
    public float attack_cooldown = 3.0f;    // Tiempo entre ataques

    float atk_cd_timer;
    bool canAttack = true;

    public Collider2D coli;

    float playerDistance;

    public float detecionRange = 10.0f;
    public float rangeToDoAttack = 3.0f;

    bool followOnBeingAttacked = false;     //Debe seguir al jugador cuando es atacado desde lejos

    [Header("Gizmos")]
    public bool dibujarRangoDeteccion = false;
    public bool dibujarRangoAtaque = false;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        player = FindObjectOfType<PlayerController>();
        animator = GetComponent<Animator>();
        coli = GetComponent<Collider2D>();
        atk_cd_timer = attack_cooldown;
        vidas.text = "Vidas:" + health;
    }

    void Update()
    {
        playerDistance = Vector2.Distance(player.transform.position - new Vector3(0, 1.5f, 0), transform.position);

        if(playerDistance <= detecionRange || followOnBeingAttacked)
        {
            animator.SetBool("Follow", true);

            if(playerDistance <= rangeToDoAttack && canAttack)
            {
                animator.SetTrigger("Attack");
            }
        }
        else
        {
            animator.SetBool("Follow", false);
        }

        if (!canAttack) AttackCooldown();
        SendHVtoAnim();
        //animator.SetFloat("PlayerDistance", Vector2.Distance(player.transform.position - new Vector3(0, 1.5f, 0), transform.position));
    }

    private void FixedUpdate()
    {
        rb.velocity = velocity * Time.fixedDeltaTime;
    }

    public void FollowPlayer()
    {
        if (!canAttack)
        {
            attacking = false;
            velocity = Vector3.zero;
            ManageSound("_cooldown");
            return;
        }

        attacking = false;
        Vector3 direction = player.transform.position - transform.position - new Vector3(0, 1.5f, 0);
        velocity = direction.normalized * speed;
        aim.transform.localPosition = rb.velocity.normalized * attackRange;

        StepSound();
    }

    public void Knockback()
    {
        attacking = false;
        Vector3 direction = transform.position - new Vector3(0, 1.5f, 0) - (Vector3)attackerPosition;
        velocity = direction.normalized * knockbackSpeed;
    }

    public void Attack()
    {
        followOnBeingAttacked = false;
        attacking = true;

        Vector3 direction = player.transform.position - transform.position - new Vector3(0, 1.5f, 0);
        velocity = direction.normalized * attack_power;

        canAttack = false;
        /*
        Collider2D player = Physics2D.OverlapCircle(aim.transform.position, attackRadius, playerLayerMask, -Mathf.Infinity, Mathf.Infinity);
        if (player)
        {
            player.GetComponent<PlayerController>().ReduceHealth(damage, transform.position);
        }*/
    }

    void AttackCooldown()
    {
        atk_cd_timer -= Time.deltaTime;

        if(atk_cd_timer <= 0.0f)
        {
            canAttack = true;
            atk_cd_timer = attack_cooldown;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (!attacking) return;

        if (collision.gameObject.CompareTag("Player"))
        {
            attacking = false;
            player.GetComponent<PlayerController>().ReduceHealth(damage, transform.position);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!attacking) return;

        if (collision.gameObject.CompareTag("Player"))
        {
            attacking = false;
            player.GetComponent<PlayerController>().ReduceHealth(damage, transform.position);
        }
    }

    public void Freeze()
    {
        attacking = false;
        velocity = Vector2.zero;
    }

    public void ReduceHealth(int damage, Vector2 position, float knockbackForce)
    {
        ManageSound("_damage");

        health -= damage;
        if(health <= 0)
        {
            Destroy(gameObject);
        }
        vidas.text = "Vidas:" + health;
        attackerPosition = position;
        knockbackSpeed = knockbackForce;
        animator.SetBool("Knockback", true);

        if (!animator.GetBool("Follow"))
        {
            followOnBeingAttacked = true;
        }
    }

    float stepTimer = 0.5f;
    float timer = 0.0f;

    void StepSound()
    {
        timer += Time.deltaTime;

        if(timer >= stepTimer)
        {
            ManageSound("_step");
            timer = 0.0f;
        }
    }

    public enum SoundType
    {
        enemy,
        enemy2
    }

    public SoundType tipo;

    public void ManageSound(string sound_event)
    {
        WwiseWrapper.SoundEvent(tipo.ToString() + sound_event, gameObject);
    }

    void SendHVtoAnim()
    {
        //if (tipo == SoundType.enemy2) return;

        try
        {
            animator.SetFloat("horizontal", rb.velocity.x);
            animator.SetFloat("vertical", rb.velocity.y);
        }
        catch
        {

        }
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (dibujarRangoAtaque)
        {
            if (rangeToDoAttack < 0.0f)
            {
                rangeToDoAttack = 0.01f;
                Debug.LogWarning("El rango de ataque no deberia ser menor a cero!");
            }

            UnityEditor.Handles.color = Color.magenta;
            UnityEditor.Handles.DrawWireDisc(transform.position, transform.forward, rangeToDoAttack);
        }

        if (dibujarRangoDeteccion)
        {
            if (detecionRange < 0.0f)
            {
                detecionRange = 0.01f;
                Debug.LogWarning("El rango de deteccion no deberia ser menor a cero!");
            }

            UnityEditor.Handles.color = Color.blue;
            UnityEditor.Handles.DrawWireDisc(transform.position, transform.forward, detecionRange);
        }
    }
#endif
}
