/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AMBIENTE = 4095160060U;
        static const AkUniqueID ARCO_ATAQUE = 2020804282U;
        static const AkUniqueID ARCO_CARGANDO = 1080128838U;
        static const AkUniqueID ATAQUE_MELEE_1 = 2784470079U;
        static const AkUniqueID ATAQUE_MELEE_2 = 2784470076U;
        static const AkUniqueID ATTACK = 180661997U;
        static const AkUniqueID BGMUSIC = 1038839729U;
        static const AkUniqueID CHAT_ACERCARSE = 2945979271U;
        static const AkUniqueID CHAT_AGUDO = 1117139388U;
        static const AkUniqueID CHAT_CROWD = 3006476517U;
        static const AkUniqueID CHAT_FIN = 2104715817U;
        static const AkUniqueID CHAT_HOMBRE = 4019894035U;
        static const AkUniqueID CHAT_MUJER = 1119971795U;
        static const AkUniqueID DAMAGE_RECEIVED = 4176958068U;
        static const AkUniqueID ENEMY_ATTACK = 1781417190U;
        static const AkUniqueID ENEMY_CHARGE = 434333010U;
        static const AkUniqueID ENEMY_COOLDOWN = 3582139221U;
        static const AkUniqueID ENEMY_DAMAGE = 555067025U;
        static const AkUniqueID ENEMY_STEP = 3434276988U;
        static const AkUniqueID ENEMY2_ATTACK = 4216730694U;
        static const AkUniqueID ENEMY2_CHARGE = 3294103986U;
        static const AkUniqueID ENEMY2_COOLDOWN = 3400463029U;
        static const AkUniqueID ENEMY2_DAMAGE = 3456414961U;
        static const AkUniqueID ENEMY2_STEP = 3746085596U;
        static const AkUniqueID FLECHA_IMPACTA = 969055488U;
        static const AkUniqueID GANCHO_DESPLEGADO = 1973361600U;
        static const AkUniqueID GANCHO_IMPACTA = 2012658321U;
        static const AkUniqueID GANCHO_RETRACTANDO = 70363567U;
        static const AkUniqueID GANCHO_VIAJANDO = 3864224318U;
        static const AkUniqueID HOOK = 3147851416U;
        static const AkUniqueID MENU = 2607556080U;
        static const AkUniqueID MENU_CLOSE = 626954121U;
        static const AkUniqueID MENU_NEWOBJ = 3083125946U;
        static const AkUniqueID MENU_OBJ = 3103987990U;
        static const AkUniqueID MENU_OPEN = 1287511387U;
        static const AkUniqueID MENU_ROLL = 240421090U;
        static const AkUniqueID MENU_SELECT = 4203375351U;
        static const AkUniqueID MUSIC_COMBAT = 3944980085U;
        static const AkUniqueID MUSIC_CONTEMPLATIVE = 3409904094U;
        static const AkUniqueID MUSIC_CONVERSATION = 658627576U;
        static const AkUniqueID MUSIC_DANGER = 1302659820U;
        static const AkUniqueID MUSIC_EMOTIVE = 2672450968U;
        static const AkUniqueID MUSIC_MAIN = 1615767906U;
        static const AkUniqueID MUSIC_SAD = 2951814051U;
        static const AkUniqueID MUSIC_SILENCE = 3010836978U;
        static const AkUniqueID PASOS = 3026462669U;
        static const AkUniqueID READ = 2579596241U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace AMBIENTE
        {
            static const AkUniqueID GROUP = 4095160060U;

            namespace STATE
            {
                static const AkUniqueID BOSQUE = 1641284458U;
                static const AkUniqueID VIENTO = 775390300U;
            } // namespace STATE
        } // namespace AMBIENTE

        namespace MENU
        {
            static const AkUniqueID GROUP = 2607556080U;

            namespace STATE
            {
                static const AkUniqueID OFF = 930712164U;
                static const AkUniqueID ON = 1651971902U;
            } // namespace STATE
        } // namespace MENU

    } // namespace STATES

    namespace SWITCHES
    {
        namespace MENU
        {
            static const AkUniqueID GROUP = 2607556080U;

            namespace SWITCH
            {
                static const AkUniqueID INGAME = 984691642U;
                static const AkUniqueID MENU = 2607556080U;
            } // namespace SWITCH
        } // namespace MENU

        namespace MUSICA_INGAME
        {
            static const AkUniqueID GROUP = 109925333U;

            namespace SWITCH
            {
                static const AkUniqueID COMBAT = 2764240573U;
                static const AkUniqueID CONTEMPLATIVE = 3961547910U;
                static const AkUniqueID CONVERSATION = 999367376U;
                static const AkUniqueID DANGER = 4174463524U;
                static const AkUniqueID EMOTIVE = 2760390432U;
                static const AkUniqueID MAIN = 3161908922U;
                static const AkUniqueID SILENCE = 3041563226U;
            } // namespace SWITCH
        } // namespace MUSICA_INGAME

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID PLAYBACK_RATE = 1524500807U;
        static const AkUniqueID RPM = 796049864U;
        static const AkUniqueID SS_AIR_FEAR = 1351367891U;
        static const AkUniqueID SS_AIR_FREEFALL = 3002758120U;
        static const AkUniqueID SS_AIR_FURY = 1029930033U;
        static const AkUniqueID SS_AIR_MONTH = 2648548617U;
        static const AkUniqueID SS_AIR_PRESENCE = 3847924954U;
        static const AkUniqueID SS_AIR_RPM = 822163944U;
        static const AkUniqueID SS_AIR_SIZE = 3074696722U;
        static const AkUniqueID SS_AIR_STORM = 3715662592U;
        static const AkUniqueID SS_AIR_TIMEOFDAY = 3203397129U;
        static const AkUniqueID SS_AIR_TURBULENCE = 4160247818U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MOTION_FACTORY_BUS = 985987111U;
        static const AkUniqueID RVB_AMBIENTE = 3674853090U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID DEFAULT_MOTION_DEVICE = 4230635974U;
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
